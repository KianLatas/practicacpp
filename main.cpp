#include <iostream>
#include <conio.h>
#include <clocale>
#include <math.h>
#define CIEN 100

int main()
{
    setlocale(LC_CTYPE, "Spanish");

    float capitalInicial = 0;
    float tasaInteres = 0;
    int tiempo = 0;
    float resultado = 0;

    while(capitalInicial<=0){
        std::cout<<"ingrese el capital inicial: "<<std::endl;
        std::cin>>capitalInicial;
    }

    while(tasaInteres<=0){
        std::cout<<"ingrese la tasa de inter�s: "<<std::endl;
        std::cin>>tasaInteres;
    }

      while(tiempo<=0){
        std::cout<<"ingrese el tiempo en a�os: "<<std::endl;
        std::cin>>tiempo;
    }

    resultado = capitalInicial * pow((1 + (tasaInteres/CIEN)),tiempo);

    std::cout<<"el dinero que se gener� es: "<<resultado<<std::endl;

    getch();
    return 0;
}
